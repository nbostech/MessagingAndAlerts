package com.wavelabs.service.test;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;
import com.wavelabs.dao.MessageDao;
import com.wavelabs.model.Message;
import com.wavelabs.model.MessageThread;
import com.wavelabs.model.User;
import com.wavelabs.service.MessageService;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTest {
	@Mock
	private MessageDao messageDao;
	@InjectMocks
	private MessageService messageService;

	@Test
	public void testPersistMessage() {
		Message msg = mock(Message.class);
		when(messageDao.saveMessage(any(Message.class))).thenReturn(true);
		Boolean flag = messageService.saveMessage(msg);
		Assert.assertEquals(true, flag);

	}

	@Test
	public void testDeleteMessageConversation() {
		when(messageDao.deleteUserConversation(anyInt(), anyInt())).thenReturn(true);
		Boolean flag = messageService.deleteUserConversation(2,4);
		Assert.assertEquals(true, flag);
	}

	@Test
	public void testGetMessagesAndUpdate() {
		User user = new User(2, "Anu", "ANu", true);
		Set<User> set = new java.util.HashSet<>();
		set.add(user);
		MessageThread mt = new MessageThread(20, true, set);
		when(messageDao.getMessagesAndUpdate(anyInt(), anyString())).thenReturn(mt);
		MessageThread m = messageService.getMessagesAndUpdate(anyInt(), anyString());
		Gson gson = new Gson();
		Assert.assertEquals(gson.toJson(mt), gson.toJson(m));

		Assert.assertEquals(mt, m);
	}
	@Test
	public void testGetConversations() {

		
	}

	@Test
	public void testGetConversationMessages() {
		@SuppressWarnings("unchecked")
		List<Message> list = mock(ArrayList.class);
		when(messageDao.getConversationMessages(anyInt())).thenReturn(list);
		List<Message> l = messageService.getConversationMessages(anyInt());
		Assert.assertEquals(list.size(), l.size());
	}
}
