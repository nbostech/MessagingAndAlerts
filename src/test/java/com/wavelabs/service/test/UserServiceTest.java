package com.wavelabs.service.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.wavelabs.dao.UserDao;
import com.wavelabs.model.User;
import com.wavelabs.service.UserService;

@RunWith(MockitoJUnitRunner.class)
@Component
public class UserServiceTest {
	@Mock
	private UserDao userDao;
	@InjectMocks
	private UserService userService;

	@Test
	public void testPersistUser() {
		User user = mock(User.class);
		when(userDao.saveUser(any(User.class))).thenReturn(true);
		Boolean flag = userService.saveUser(user);
		Assert.assertEquals(true, flag);
	}

	@Test
	public void testDeleteUser() {
		when(userDao.deleteUser(anyInt())).thenReturn(true);
		Boolean flag = userService.deleteUser(2);
		Assert.assertEquals(true, flag);
	}

	@Test
	public void testGetUser() {
		User user = new User(4, "AA", "a", true);
		when(userDao.getUser(anyInt())).thenReturn(user);
		User u = userService.getUser(2);
		Gson gson = new Gson();
		Assert.assertEquals(gson.toJson(user), gson.toJson(u));
	}

}
