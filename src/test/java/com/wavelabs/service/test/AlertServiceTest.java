package com.wavelabs.service.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;
import com.wavelabs.dao.AlertDao;
import com.wavelabs.model.Alert;
import com.wavelabs.model.AlertMetadata;
import com.wavelabs.model.Status;
import com.wavelabs.model.User;
import com.wavelabs.service.AlertService;

@RunWith(MockitoJUnitRunner.class)
public class AlertServiceTest {
	@Mock
	private AlertDao alertDao;
	@InjectMocks
	private AlertService alertService;

	@Test
	public void testPersistAlert() {
		Alert alert = mock(Alert.class);
		when(alertDao.saveAlert(any(Alert.class))).thenReturn(true);
		Boolean flag = alertService.saveAlert(alert);
		Assert.assertEquals(true, flag);
	}

	@Test
	public void testGetAlert() {
		User user = new User(2, "Anu", "ANu", true);
		Set<User> set = new java.util.HashSet<>();
		set.add(user);
		AlertMetadata am = new AlertMetadata();
		Alert alert = new Alert(3, user, user, "Request sent", am, Status.VIEWED );
		when(alertDao.getAlert(anyInt())).thenReturn(alert);
		Alert a = alertService.getAlert(anyInt());
		Gson gson = new Gson();
		Assert.assertEquals(gson.toJson(alert), gson.toJson(a));
	}

	@Test
	public void testGetAlertAndUpdate() {
		User user = new User(2, "Anu", "ANu", true);
		Set<User> set = new java.util.HashSet<>();
		set.add(user);
		AlertMetadata am = new AlertMetadata();
		Alert alert = new Alert(3, user, user, "Request sent", am, Status.ACCEPT );
		when(alertDao.getAlertAndUpdate(anyInt(), anyString())).thenReturn(alert);
		Alert a = alertService.getAlertAndUpdate(anyInt(), anyString());
		Gson gson = new Gson();
		Assert.assertEquals(gson.toJson(alert), gson.toJson(a));
	}

	@Test
	public void testgetAllAlertsPerReceiver() {
		@SuppressWarnings("unchecked")
		List<Alert> list = mock(ArrayList.class);
		when(alertDao.getAllAlertsPerReceiver(anyInt())).thenReturn(list);
		List<Alert> l = alertService.getAllAlertsPerReceiver(anyInt());
		Assert.assertEquals(list.size(), l.size());
	}

}
