package com.wavelabs.resource.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.api.UserController;
import com.wavelabs.model.User;
import com.wavelabs.service.UserService;
@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {
	@Mock
	private UserService userService;
	@InjectMocks
	private UserController userResource;
	
	
	@Test
	public void testPersistUser(){
		User user = new User(3, "AAA", "M", true);
		when(userService.saveUser(any(User.class))).thenReturn(true);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = userResource.createUser(user);
		assertEquals(200,res.getStatusCodeValue());
		
		
	}
	@Test
	public void testPersistUser1(){

		User user = new User(4, "AAA", "M", true);
		when(userService.saveUser(any(User.class))).thenReturn(false);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = userResource.createUser(user);
		assertEquals(500,res.getStatusCodeValue());
		
		
	}

@Test
	public void testGetUser(){
		User user = new User(44, "Anu", "ANu", true);
		when(userService.getUser(anyInt())).thenReturn(user);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = userResource.getUser(4);
		assertEquals("is not matched", 200, res.getStatusCodeValue());
	}
	@Test
	public void testgetUser1(){
		when(userService.getUser(anyInt())).thenReturn(null);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = userResource.getUser(4);
		assertEquals("is not matched", 500, res.getStatusCodeValue());
	}
	@Test
	public void testDeleteUser(){
		when(userService.deleteUser(anyInt())).thenReturn(true);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = userResource.deleteUser(5);
		assertEquals(200, res.getStatusCodeValue());
		
	}
	@Test
	public void testDeleteUser1(){
		when(userService.deleteUser(anyInt())).thenReturn(false);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = userResource.deleteUser(8);
		assertEquals(500, res.getStatusCodeValue());
	}

}
