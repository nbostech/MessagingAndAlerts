package com.wavelabs.resource.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.api.AlertController;
import com.wavelabs.model.Alert;
import com.wavelabs.model.AlertMetadata;
import com.wavelabs.model.Status;
import com.wavelabs.model.User;
import com.wavelabs.service.AlertService;

@RunWith(MockitoJUnitRunner.class)
public class AlertControllerTest {
	@Mock
	private AlertService alertService;
	@InjectMocks
	private AlertController alertResource;

	@Test
	public void testPersistAlert() {
		User user = new User(1, "Janu", "Janu", true);
		AlertMetadata am = new AlertMetadata();
		Alert alert = new Alert(3, user, user, "Contact sent", am, Status.VIEWED);
		when(alertService.saveAlert(any(Alert.class))).thenReturn(true);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = alertResource.saveAlert(alert);
		assertEquals("not matched", 200, res.getStatusCodeValue());
	}

	@Test
	public void testPersistAlertIsNull() {
		User user = new User(1, "Janu", "Janu", true);
		AlertMetadata am = new AlertMetadata();
		Alert alert = new Alert(3, user, user, "Contact sent", am, Status.VIEWED);
		when(alertService.saveAlert(any(Alert.class))).thenReturn(false);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = alertResource.saveAlert(alert);
		assertEquals("not matched", 500, res.getStatusCodeValue());
	}

	@Test
	public void testGetAlert() {
		User user = new User(1, "Janu", "Janu", true);
		AlertMetadata am = new AlertMetadata();
		Alert alert = new Alert(3, user, user, "Contact sent", am, Status.VIEWED);
		when(alertService.getAlert(anyInt())).thenReturn(alert);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = alertResource.getAlert(anyInt());
		assertEquals("not matched", 200, res.getStatusCodeValue());

	}

	@Test
	public void testGetAlertIsNull() {
		when(alertService.saveAlert(any(Alert.class))).thenReturn(null);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = alertResource.getAlert(anyInt());
		assertEquals("not matched", 500, res.getStatusCodeValue());

	}

	@Test
	public void testGetAllAlertsAsPerReceiver() {
		User user = new User(1, "Janu", "Janu", true);
		AlertMetadata am = new AlertMetadata();
		Alert alert = new Alert(3, user, user, "Contact sent", am, Status.VIEWED);
		Alert alert1 = new Alert(3, user, user, "Contact sent", am, Status.DECLINE);
		List<Alert> list = new ArrayList<>();
		list.add(alert);
		list.add(alert1);
		when(alertService.getAllAlertsPerReceiver(anyInt())).thenReturn(list);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = alertResource.getAllAlertsPerReceiver(anyInt());
		assertEquals("not matched", 200, res.getStatusCodeValue());
	}

	@Test
	public void testGetAllAlertsAsPerReceiverIsEmpty() {
		User user = new User(1, "Janu", "Janu", true);
		AlertMetadata am = new AlertMetadata();
		Alert alert = new Alert(3, user, user, "Contact sent", am, Status.VIEWED);
		Alert alert1 = new Alert(3, user, user, "Contact sent", am, Status.DECLINE);
		List<Alert> list = new ArrayList<>();
		list.add(alert);
		list.add(alert1);
		when(alertService.getAllAlertsPerReceiver(anyInt())).thenReturn(Collections.emptyList());
		@SuppressWarnings("rawtypes")
		ResponseEntity res = alertResource.getAllAlertsPerReceiver(anyInt());
		assertEquals("not matched", 500, res.getStatusCodeValue());
	}

	@Test
	public void testGetAlertAndUpdate() {
		User user = new User(1, "Janu", "Janu", true);
		AlertMetadata am = new AlertMetadata();
		Alert alert = new Alert(3, user, user, "Contact sent", am, Status.VIEWED);
		when(alertService.getAlertAndUpdate(anyInt(), anyString())).thenReturn(alert);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = alertResource.getAlertAndUpdate(2, "accept");
		assertEquals("not matched", 200, res.getStatusCodeValue());

	}

	@Test
	public void testGetAlertAndUpdateIsNull() {
		when(alertService.getAlertAndUpdate(anyInt(), anyString())).thenReturn(null);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = alertResource.getAlertAndUpdate(2, "accept");
		assertEquals("not matched", 500, res.getStatusCodeValue());

	}
}
