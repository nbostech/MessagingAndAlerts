package com.wavelabs.resource.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.api.MessageController;
import com.wavelabs.model.Message;
import com.wavelabs.model.MessageThread;
import com.wavelabs.model.User;
import com.wavelabs.service.MessageService;

@RunWith(MockitoJUnitRunner.class)
public class MessageControllerTest {
	@Mock
	MessageService messageService;
	@InjectMocks
	MessageController messageResource;
@Test
	public void testPersistMessage() {
		User user = new User(3, "AAA", "aa", true);
		Set<User> set = new HashSet<>();
		set.add(user);
		MessageThread mt = new MessageThread(1, true, set);
		Message m = new Message(2, "Hai", "hey.", false, user, user, mt);
		when(messageService.saveMessage(m)).thenReturn(true);
		@SuppressWarnings("rawtypes")
		ResponseEntity res = messageResource.createMessage(m);
		assertEquals("not matched", 200, res.getStatusCodeValue());
	}

}
