package com.wavelabs.model.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.wavelabs.model.AlertMetadata;
import com.wavelabs.model.AlertType;

public class AlertMetadataTest {
	
	@Test
	public void testDefaultConstructor(){
		AlertMetadata am = new AlertMetadata();
		am.setId(3);
		assertEquals(3, am.getId());
		am.setAlertType(AlertType.VIEWONLY);
		assertEquals( am.getAlertType().equals("VIEWONLY"), false);
		
	}
	@Test
	public void testParameterizedConstructor(){
		AlertMetadata am = new AlertMetadata(2, AlertType.VIEWONLY);
		assertEquals("not given", am.getId(), 2);
		assertEquals("not given", am.getAlertType().equals("VIEWONLY"), false);
		
		
	}
	
	@Test
	public void testId(){
		AlertMetadata am = new AlertMetadata();
		am.setId(2);
		assertEquals("didn't match id", am.getId() == 2, true);
		
	}
	@Test
	public void testAlertType(){
		AlertMetadata am = new AlertMetadata();
		am.setAlertType(AlertType.VIEWONLY);
		assertEquals("didn't match", am.getAlertType().equals("VIEWONLY"), false);
		am.setAlertType(AlertType.VIEWDESTINATION);
		assertEquals("didn't match", am.getAlertType().equals("VIEWDESTINATION"), false);
		am.setAlertType(AlertType.ACCEPTDECLINE);
		assertEquals("didn't match", am.getAlertType().equals("ACCEPTDECLINE"), false);
		
		
	}

}
