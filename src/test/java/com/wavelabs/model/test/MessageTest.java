package com.wavelabs.model.test;


import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;

import org.junit.Test;

import com.wavelabs.model.Message;
import com.wavelabs.model.MessageThread;
import com.wavelabs.model.User;


public class MessageTest {
	
	Message msg = new Message();
	User user = new User();
	MessageThread mt = new MessageThread();
	@Test
	public void testDefaultConsturctor(){
	    msg.setFromId(user);
	    assertEquals(user,msg.getFromId());
	    msg.setTo_Id(user);;
	    assertEquals(user,msg.getTo_Id());
	    msg.setBody("Hi");
	    assertEquals("Hi",msg.getBody());
	    msg.setSubject("Hey");
	    assertEquals("Hey",msg.getSubject());
	    msg.setMessagethread(mt);
	    assertEquals(mt,msg.getMessagethread());
	    msg.setIsreply(false);
	    assertEquals(false, false);
	}
	@Test
	public void testParameterizedConstructor(){
		Message msg = new Message(3, "Hai", "This is message", false, user, user, mt);
		assertEquals("not given", msg.getId(), 3);
		assertEquals("not given", msg.getFromId(), user);
		assertEquals("not given", msg.getTo_Id(), user);
		assertEquals("not given", msg.getBody(), "This is message");
		assertEquals("not given", msg.getSubject(),"Hai");
		assertEquals("not given", msg.getMessagethread(), mt);
		
		
	}
	@Test
	public void testId() {
		msg.setId(2);
		assertEquals("Didn't match",msg.getId() == 2, true);
	}
	@Test
	public void testBody(){
		msg.setBody("Hello");
		assertEquals("didn't match", msg.getBody() == "Hello", true);
		
	}
	@Test
	public void testSubject(){
		msg.setSubject("Hi");
		assertEquals("didn't match", msg.getSubject() == "Hi", true);
		
	}
	@Test
	public void testFromId(){
		msg.setFromId(user);
		assertEquals("didn't match", msg.getFromId() == user, true);
		
	}
	@Test
	public void testToId(){
		msg.setTo_Id(user);;
		assertEquals("didn't match", msg.getTo_Id() == user, true);
		
	}
	@Test
	public void testMessageThread(){
		msg.setMessagethread(mt);
		assertEquals("didn't match", msg.getMessagethread() == mt, true);
		
	}
		
	@Test
	public void testTimeStamp(){
		
		Timestamp timestamp = Timestamp.valueOf("2007-12-23 09:01:06.000000003");
		 assertEquals("not given", timestamp.equals("2007-12-23 09:01:06.000000003"), false);
	}
	@Test
	public void testIsreply(){
		msg.setIsreply(false);
		assertEquals("not given", msg.isIsreply() == false, true);
		
	}
}
