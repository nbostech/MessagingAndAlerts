package com.wavelabs.model.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.wavelabs.model.User;

public class UserTest {
	/*
	 * @Test public void testSetFirstName() throws NoSuchFieldException,
	 * SecurityException, IllegalArgumentException, IllegalAccessException{
	 * final User user = new User(); //when user.setFirstName("AAA"); //then
	 * final Field field = user.getClass().getDeclaredField("AAA");
	 * field.setAccessible(true); assertEquals("Fields didn't match",
	 * field.get(user),"AAA"); assertEquals(true,
	 * user.getFirstName().equals(user)); }
	 */
	@Test
	public void testDefaultConstructor() {
		User user = new User();
		user.setFirstName("Thejasree");
		assertEquals("Thejasree", user.getFirstName());
		user.setLastName("Manyam");
		assertEquals("Manyam", user.getLastName());
		user.setId(2);
		assertEquals(2, user.getId());
		user.setIsactive(true);
		assertEquals(true, true);

	}
	@Test
	public void testParameterizedConstructor(){
		User user = new User(2, "Anu", "Anu", true);
		assertEquals("not given", user.getId(), 2);
		assertEquals("not given", user.getFirstName(), "Anu");
		assertEquals("not given", user.getLastName(), "Anu");
		assertEquals("not given", user.isIsactive(), true);
	}

	@Test
	public void testSetFirstName() {
		User user = new User();
		user.setFirstName("AAA");
		assertEquals("Didn't match", user.getFirstName() == "AAA", true);
	}

	@Test
	public void testSetId() {
		User user = new User();
		user.setId(2);
		assertEquals("didn't match", user.getId() == 2, true);
	}

	@Test
	public void testSetLastName() {
		User user = new User();
		user.setLastName("A");
		assertEquals("Didn't match", user.getLastName() == "A", true);

	}
@Test
public void testIsActive(){
	User user = new User();
	user.setIsactive(true);
	assertEquals("not given", user.isIsactive() == true, true);
}
}
