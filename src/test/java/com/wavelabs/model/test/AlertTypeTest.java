package com.wavelabs.model.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.wavelabs.model.AlertType;

public class AlertTypeTest {
	@Test
	public void testAlertType() {
		assertThat(AlertType.valueOf("ACCEPTDECLINE"), is(notNullValue()));
		assertThat(AlertType.valueOf("VIEWONLY"), is(notNullValue()));
		assertThat(AlertType.valueOf("VIEWDESTINATION"), is(notNullValue()));
	}

}
