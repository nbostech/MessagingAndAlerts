package com.wavelabs.model.test;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.wavelabs.model.MessageThread;
import com.wavelabs.model.User;

public class MessageThreadTest {
	MessageThread mt = new MessageThread();
	User user = new User();
	

	@Test
	public void testDefaultConstructor(){
		mt.setId(3);
		assertEquals("not given", mt.getId() ==2, false);
		mt.setIsread(true);
		assertEquals("not given",mt.isIsread() == true, true);
		Set<User> set = new HashSet<User>();
		set.add(user);
		mt.setUser(set);
		assertEquals("not given",mt.getUser() , set);
		
	}
	@Test
	public void testParameterizedConstructor(){
		Set<User> set = new HashSet<User>();
		set.add(user);
		MessageThread mt = new MessageThread(3, true, set);
		assertEquals("not given", mt.getId(), 3);
		assertEquals("not given", mt.getUser(), set);
		assertEquals("not given", mt.isIsread(), true);
	}
	@Test
	public void testId(){
		mt.setId(2);
		assertEquals("not given", mt.getId() == 2, true);
		
	}
	@Test
	public void testIsread(){
		mt.setIsread(true);
		assertEquals("not given", mt.isIsread() == true, true);
		
	}
	/*@Test
	public void testUsers(){
		mt.setUser(user);
		
	}*/
	

}
