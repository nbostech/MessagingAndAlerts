package com.wavelabs.model.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.wavelabs.model.ErrorMessage;

public class ErrorMessageTest {
	
	@Test
	public void testDefaultConstructor(){
		ErrorMessage em = new ErrorMessage();
		em.setId(2);
		assertEquals("not given", em.getId(), 2);
		em.setErrormessage("Not found");
		assertEquals("not given", em.getErrormessage(), "Not found");
		
	}
	@Test
	public void testParameterizedConstructor(){
		ErrorMessage em = new ErrorMessage(2,"value not found");
		assertEquals(em.getId(),2);
		assertEquals(em.getErrormessage(),"value not found");
		
		
		
	}
	@Test
	public void testId(){
		ErrorMessage em = new ErrorMessage();
		em.setId(3);
		assertEquals("ndidn't match", em.getId(), 3);
		
		
	}
	@Test
	public void testErrorMessage(){
		ErrorMessage em = new ErrorMessage();
		em.setErrormessage("Resource not found");
		assertEquals("ndidn't match", em.getErrormessage() == "Resource not found", true);
		
		
	}

}
