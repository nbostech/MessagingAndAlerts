package com.wavelabs.api;

import java.util.List;

import org.hibernate.boot.jaxb.hbm.internal.GenerationTimingConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.model.ErrorMessage;
import com.wavelabs.model.Message;
import com.wavelabs.model.MessageThread;
import com.wavelabs.service.MessageService;

@RestController
@Component
public class MessageController {
	@Autowired
	private MessageService messageService;

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "messages", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity createMessage(@RequestBody Message message) {
		Boolean flag = messageService.saveMessage(message);
		ErrorMessage em = new ErrorMessage();
		if (flag) {
			em.setId(200);
			em.setErrormessage("Message creation success!");
			return ResponseEntity.status(200).body(em);
		} else {
			em.setErrormessage("Message creation failed!");
			return ResponseEntity.status(500).body(em);

		}

	}
<<<<<<< HEAD:src/main/java/com/wavelabs/api/MessageController.java

	/*
	 * @RequestMapping(value = "/conversations/{id}" ,method =
	 * RequestMethod.GET) public Message[] getConversations(@PathVariable("id")
	 * Integer id) {
	 * 
	 * return messageService.getConversations(id); }
	 */

=======
>>>>>>> c1697d7a04e74d92fbabe1f1329bd26c407523f4:src/main/java/com/wavelabs/controller/MessageController.java
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/conversations/{id}", method = RequestMethod.GET)
	public ResponseEntity getConversations(@PathVariable("id") Integer id) {
		Message[] msgs = messageService.getConversations(id);
		if (msgs.length != 0) {
			return ResponseEntity.status(200).body(msgs);
		} else {
			ErrorMessage messge = new ErrorMessage();
			messge.setId(404);
<<<<<<< HEAD:src/main/java/com/wavelabs/api/MessageController.java
			messge.setErrormessage("bad request");
			return ResponseEntity.status(404).body(msgs);
=======
			messge.setErrormessage("Not found");
			return ResponseEntity.status(404).body(messge);
>>>>>>> c1697d7a04e74d92fbabe1f1329bd26c407523f4:src/main/java/com/wavelabs/controller/MessageController.java
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/list/{id}", method = RequestMethod.GET)
	public ResponseEntity getConversationMessages(@PathVariable("id") Integer id) {

		List<Message> msg = messageService.getConversationMessages(id);
		if (msg.isEmpty()) {
			ErrorMessage em = new ErrorMessage();
			em.setId(500);
			em.setErrormessage("Failed to retieve!");
			return ResponseEntity.status(500).body(em);
		}
		ErrorMessage em = new ErrorMessage();
		em.setId(200);
		em.setErrormessage("successs!");
		return ResponseEntity.status(200).body(msg);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/messages/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getMessagesAndUpdate(@PathVariable("id") Integer id, @RequestParam String isRead) {
		MessageThread mt = messageService.getMessagesAndUpdate(id, isRead);
		ErrorMessage em = new ErrorMessage();
		if (mt == null) {
			em.setId(500);
			em.setErrormessage("updation failed!");
			return ResponseEntity.status(500).body(em);
		}
		em.setId(200);
<<<<<<< HEAD:src/main/java/com/wavelabs/api/MessageController.java
		em.setErrormessage("updation success!");
=======
		em.setErrormessage("Alert updation success!");
>>>>>>> c1697d7a04e74d92fbabe1f1329bd26c407523f4:src/main/java/com/wavelabs/controller/MessageController.java
		return ResponseEntity.status(200).body(mt);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "{userId}/{messageThreadId}", method = RequestMethod.DELETE)
	public ResponseEntity deleteUserConversation(@PathVariable("userId") Integer userId,
			@PathVariable("messageThreadId") int messageThreadId) {
		Boolean flag = messageService.deleteUserConversation(userId, messageThreadId);
		ErrorMessage em = new ErrorMessage();
		if (flag) {
			em.setId(200);
			em.setErrormessage("success!");
			return ResponseEntity.status(200).body(flag);
		} else {
			em.setId(500);
			em.setErrormessage("failed!");
			return ResponseEntity.status(500).body(em);

		}
	}
		
		@SuppressWarnings("rawtypes")
		@RequestMapping(value = "/message/{userId}", method = RequestMethod.DELETE)
		public ResponseEntity deleteMessage(@PathVariable("id") Integer id) {
			Boolean flag = messageService.deleteMessage(id);
			ErrorMessage em = new ErrorMessage();
			if (flag) {
				em.setId(200);
				em.setErrormessage("Deletion Success!");
				return ResponseEntity.status(200).body(flag);
			} else {
				em.setId(500);
				em.setErrormessage("Deletion Failed!");
				return ResponseEntity.status(500).body(em);

			}
	}
}