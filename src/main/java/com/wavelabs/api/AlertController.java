package com.wavelabs.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.model.Alert;
import com.wavelabs.model.ErrorMessage;
import com.wavelabs.service.AlertService;

@RestController
@Component
public class AlertController {
	@Autowired
	private AlertService alertService;

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/alerts", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity saveAlert(@RequestBody Alert alert) {
		Boolean flag = alertService.saveAlert(alert);
		if (flag) {
			ErrorMessage em = new ErrorMessage();
			em.setId(200);
			em.setErrormessage("Alert is created successfully!");
			return ResponseEntity.status(200).body(em);
		} else {
			ErrorMessage em = new ErrorMessage();
			em.setId(500);
			em.setErrormessage("Creation failed!");
			return ResponseEntity.status(500).body(em);

		}

	}

	@SuppressWarnings("rawtypes")
	@RequestMapping("a/{id}")
	public ResponseEntity getAlert(@PathVariable("id") Integer id) {
		Alert alert = alertService.getAlert(id);
		if (alert == null) {
			ErrorMessage em = new ErrorMessage();
			em.setId(500);
			em.setErrormessage(" failed!");
			return ResponseEntity.status(500).body(em);
		}
		ErrorMessage em = new ErrorMessage();
		em.setId(200);
		em.setErrormessage("Alert retrieval success!");
		return ResponseEntity.status(200).body(alert);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/receivedAlerts/{id}")

	public ResponseEntity getAllAlertsPerReceiver(@PathVariable("id") Integer id) {
		List<Alert> alert = alertService.getAllAlertsPerReceiver(id);
		if (alert.isEmpty()) {
			ErrorMessage em = new ErrorMessage();
			em.setId(500);
			em.setErrormessage("Alert retrieval failed!");
			return ResponseEntity.status(500).body(em);
		}
		ErrorMessage em = new ErrorMessage();
		em.setId(200);
		em.setErrormessage("Alert retrieval failed!");
		return ResponseEntity.status(200).body(alert);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/alerts/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getAlertAndUpdate(@PathVariable("id") Integer id, @RequestParam String status) {
		Alert alert = alertService.getAlertAndUpdate(id, status);
		if (alert == null) {
			ErrorMessage em = new ErrorMessage();
			em.setId(500);
			em.setErrormessage("Alert updation failed!");
			return ResponseEntity.status(500).body(em);
		}
		ErrorMessage em = new ErrorMessage();
		em.setId(200);
		em.setErrormessage("Alert updation success!");
		return ResponseEntity.status(200).body(alert);
	}
}
