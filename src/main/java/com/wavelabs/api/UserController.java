package com.wavelabs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.model.ErrorMessage;
import com.wavelabs.model.User;
import com.wavelabs.service.UserService;

@RestController
@Component
public class UserController {
	@Autowired
	private UserService userService;

	private UserController(UserService userService) {
		this.userService = userService;
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/users", method = org.springframework.web.bind.annotation.RequestMethod.POST)
	public ResponseEntity createUser(@RequestBody User user) {

		Boolean flag = userService.saveUser(user);
		ErrorMessage em = new ErrorMessage();
		if (flag) {
			em.setId(200);
			em.setErrormessage("User is successfully created!!");
			return ResponseEntity.status(200).body(em);
		} else {
			em.setId(500);
			em.setErrormessage("User is not created!!");
			return ResponseEntity.status(500).body(em);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping("/users/{id}")
	public ResponseEntity getUser(@PathVariable("id") Integer id) {
		User user = userService.getUser(id);
		ErrorMessage em = new ErrorMessage();
		if (user == null) {
			em.setId(500);
			em.setErrormessage("Failed, try again!!");
			return ResponseEntity.status(500).body(em);

		}
		em.setId(200);
		em.setErrormessage("User is successfully retrieved!!");
		return ResponseEntity.status(200).body(user);
	}

	@SuppressWarnings("rawtypes")
<<<<<<< HEAD:src/main/java/com/wavelabs/api/UserController.java
	@RequestMapping(value = "/user/{id}", method = org.springframework.web.bind.annotation.RequestMethod.DELETE)
=======
	@RequestMapping(value = "/users/{id}", method = org.springframework.web.bind.annotation.RequestMethod.DELETE)
>>>>>>> c1697d7a04e74d92fbabe1f1329bd26c407523f4:src/main/java/com/wavelabs/controller/UserController.java
	public ResponseEntity deleteUser(@PathVariable("id") Integer id) {
		Boolean flag = userService.deleteUser(id);
		ErrorMessage em = new ErrorMessage();
		if (flag) {
			em.setId(200);
			em.setErrormessage("User is successfully deleted!!");

			return ResponseEntity.status(200).body(em);
		} else {
			em.setId(500);
			em.setErrormessage("failed!!");
			return ResponseEntity.status(500).body(em);
		}

	}

}
