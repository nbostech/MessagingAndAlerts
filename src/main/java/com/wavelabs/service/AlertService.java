package com.wavelabs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.dao.AlertDao;
import com.wavelabs.model.Alert;

@Service
public class AlertService {
	@Autowired
	private AlertDao service;
	
	
	private AlertService(AlertDao service){
		this.service = service;
	}

	public  Boolean saveAlert(Alert alert) {
		return service.saveAlert(alert);
	}

	public  Alert getAlert(int id) {
		return service.getAlert(id);
	}

	public  List<Alert> getAllAlertsPerReceiver(int id) {
		return service.getAllAlertsPerReceiver(id);
	}

	public  Alert getAlertAndUpdate(int id, String status) {
		return service.getAlertAndUpdate(id, status);
	}
}
