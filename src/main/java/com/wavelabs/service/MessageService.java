package com.wavelabs.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.dao.MessageDao;
import com.wavelabs.model.Message;
import com.wavelabs.model.MessageThread;
@Service
public class MessageService {
	@Autowired
	private MessageDao messageDao;
	static Logger logger = Logger.getLogger(MessageService.class);

	private MessageService() {

	}

	public  Message[] getConversations(int id) {
		return messageDao.getConversations(id);

	}

	public  List<Message> getConversationMessages(int id) {
		return messageDao.getConversationMessages(id);

	}

	public  MessageThread getMessagesAndUpdate(int id, String isRead) {
		return messageDao.getMessagesAndUpdate(id, isRead);
	}

	public  Boolean saveMessage(Message message) {
		return messageDao.saveMessage(message);
	}

	public  Boolean deleteUserConversation(int userId, int messageThreadId) {
		return messageDao.deleteUserConversation(userId, messageThreadId);

	}

	public Boolean deleteMessage(Integer id) {
		return messageDao.deleteMessage(id);
	}

}
