package com.wavelabs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.dao.UserDao;
import com.wavelabs.model.User;
@Service
public class UserService {
	@Autowired
	private UserDao userDao;
	private UserService(UserDao userDao){
		this.userDao = userDao;
	}
	
	public  Boolean saveUser(User user) {
		return userDao.saveUser(user);
	}

	public  User getUser(int id) {
		return userDao.getUser(id);
	}

	public  Boolean deleteUser(int id) {
		return userDao.deleteUser(id);
	}

}
