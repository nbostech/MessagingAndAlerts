package com.wavelabs.model;

import java.util.Set;

public class MessageThread {
	private int id;
	private boolean isread;
	private Set<User> user;
	public MessageThread(){
		
	}

	public MessageThread(int id, boolean isread, Set<User> user) {
		super();
		this.id = id;
		this.isread = isread;
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isIsread() {
		return isread;
	}

	public void setIsread(boolean isread) {
		this.isread = isread;
	}

	public Set<User> getUser() {
		return user;
	}

	public void setUser(Set<User> user) {
		this.user = user;
	}

}
