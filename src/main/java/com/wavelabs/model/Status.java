package com.wavelabs.model;

public enum Status {
	ACCEPT, DECLINE, VIEWED;
}
