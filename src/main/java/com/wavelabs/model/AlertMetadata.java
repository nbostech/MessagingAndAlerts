package com.wavelabs.model;

public class AlertMetadata {
	private int id;
	private AlertType alertType;

	public AlertMetadata() {

	}

	public AlertMetadata(int id, AlertType alertType) {
		super();
		this.id = id;
		this.alertType = alertType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public AlertType getAlertType() {
		return alertType;
	}

	public void setAlertType(AlertType alertType) {
		this.alertType = alertType;
	}

}
