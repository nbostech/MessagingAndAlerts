package com.wavelabs.model;

public class ErrorMessage {
	private int id;
	private String errormessage;

	public ErrorMessage() {

	}

	public ErrorMessage(int id, String errormessage) {
		super();
		this.id = id;
		this.errormessage = errormessage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getErrormessage() {
		return errormessage;
	}

	public void setErrormessage(String errormessage) {
		this.errormessage = errormessage;
	}

}
