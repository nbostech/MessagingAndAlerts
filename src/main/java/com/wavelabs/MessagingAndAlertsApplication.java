package com.wavelabs;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
<<<<<<< HEAD
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;
=======
>>>>>>> c1697d7a04e74d92fbabe1f1329bd26c407523f4

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
<<<<<<< HEAD
@ComponentScan({ "com.wavelabs.api", "com.wavelabs.service", "com.wavelabs.dao" })
=======
@ComponentScan({ "com.wavelabs.controller", "com.wavelabs.service", "com.wavelabs.dao" })
>>>>>>> c1697d7a04e74d92fbabe1f1329bd26c407523f4
public class MessagingAndAlertsApplication {
	/**
	 * Method that Spring Boot detects when you run the executable jar file.
	 * 
	 * @param args
	 * @throws Exception
	 */

	public static void main(String[] args) {
		SpringApplication.run(MessagingAndAlertsApplication.class, args);

	}

	/**
	 * Enables Spring's annotation-driven cache management capability, similar
	 * to the support found in Spring's <cache:*> XML namespace.
	 * 
	 * @return GuavaCacheManager
	 */
/*	@Bean
	public CacheManager cacheManager() {
		GuavaCacheManager cacheManager = new GuavaCacheManager("greetings");
		return cacheManager;
	}*/

	/**
	 * 
	 * {@link ServletRegistrationBean servletRegistrationBean} is class with a
	 * spring bean friendly design that is used to register servlets in a
	 * servlet 3.0 container within spring boot. Register dispatcherServlet
	 * programmatically
	 * 
	 * @return ServletRegistrationBean
	 */
	@Bean
	public ServletRegistrationBean dispatcherServletRegistration() {
		ServletRegistrationBean registration = new ServletRegistrationBean(dispatcherServlet(), "/*");
		registration.setName(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME);
		return registration;
	}

	/**
	 * To add your own servlet and map it to the root resource. Default would be
	 * root of your application(/)
	 * 
	 * @return DispatcherServlet
	 */
	@Bean
	public DispatcherServlet dispatcherServlet() {
		return new DispatcherServlet();
	}

	/**
	 * Method to set the order for the filters
	 * 
	 * @return
	 */
	@Bean
	public Filter springFilter() {
		return new SpringFilter();
	}
	@Bean
	public AuthenticationTokenCache authenticationTokenCache() {
		
		return new AuthenticationTokenCache();
		
	}
}
