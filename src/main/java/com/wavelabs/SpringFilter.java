package com.wavelabs;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.google.gson.Gson;
import com.wavelabs.model.ErrorMessage;
import com.wavelabs.model.Modules;
import com.wavelabs.model.UserTokenInfo;
import com.wavelabs.utils.Constants;

/**
 * This the Filter class to filter all the User requests for Auth tokens. This
 * would validate if the requested URL required user login. If it requires user
 * login, this class would authenticate the request with NBOS.in
 * 
 *
 */
@Component
public class SpringFilter implements Filter {
	@Autowired
	private Environment env;
	private static final String moduleToken = "MOD:69f2fd55-799e-4e35-a624-d2af946d0bdb";

	private static Logger logger = LoggerFactory.getLogger(SpringFilter.class);

	private AuthenticationTokenCache authTokenCache = AuthenticationTokenCache.getInstance();

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
	}

	/**
	 * Method to filter all the user requests and authenticate them
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 *      javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		final HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		System.out.println(httpServletRequest.getRequestURI());
		if (needLogin(httpServletRequest)) {
			String accessToken = httpServletRequest.getHeader(Constants.ACCESS_TOKEN);
			String url = env.getProperty(Constants.AUTH_URL);
			String moduleKey = env.getProperty(Constants.MODULE_KEY_LABEL);
			String authorization = Constants.BEARER + env.getProperty(Constants.VERIFY_TOKEN);
			Assert.notNull(accessToken, "Access token cannot be empty");
			Assert.notNull(url, "NBOS.in url cannot be empty");
			Assert.notNull(moduleKey, "Module Key cannot be empty");
			Assert.notNull(env.getProperty(Constants.VERIFY_TOKEN), "Verify Token cannot be empty");
			logger.info("validating the user Authentication Token");
			if (authTokenCache.validateAuthToken(request, accessToken, authorization, moduleKey, url)) {
				UserTokenInfo userTokenInfo = (UserTokenInfo) request.getAttribute("userTokenInfo");
				Boolean flag = checkSubscription(userTokenInfo);
				if (flag) {
					 chain.doFilter(httpServletRequest, httpServletResponse);
				} else {
					HttpServletResponse httpResponse = (HttpServletResponse) response;
					ErrorMessage em = new ErrorMessage();
					em.setId(401);
					em.setErrormessage("You aren't subscribed to the module...please check it...");
					httpResponse.sendError(403, objectToJsonConverter(em));
				}

			}
		} else {
			chain.doFilter(request, response);
		}

	}

	private Boolean checkSubscription(UserTokenInfo userTokenInfo) {
		Boolean flag = false;
		Modules[] modules = userTokenInfo.getModules();
		for (Modules m : modules) {
			if (m.getUuid().equals(moduleToken)) {
				 return true;
			}
		}
		return flag;
	}

	/**
	 * check if the user request needs login, based on the URL
	 * 
	 * @param request
	 * @return
	 */
	private Boolean needLogin(final HttpServletRequest httpServletRequest) {
		Boolean needed = Boolean.TRUE;
		String requestURI = httpServletRequest.getRequestURI();
		if (requestURI.indexOf("/visitor/") > 0) {
			needed = Boolean.FALSE;
		}
		return needed;
	}

	/**
	 * Method to proceed to the next element in the chain
	 * 
	 * @param request
	 * @param response
	 */
	@SuppressWarnings("unused")
	private void processRequest(ServletRequest request, ServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		// call the underlying rest servlet
		chain.doFilter(request, response);
	}

	public String objectToJsonConverter(ErrorMessage response) {
		Gson gson = new Gson();
		return gson.toJson(response);
	}

}
