package com.wavelabs.dao;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.wavelabs.model.Alert;
import com.wavelabs.model.Status;
import com.wavelabs.model.User;
import com.wavelabs.utility.Helper;
@Component
public class AlertDao {
	static Logger logger = Logger.getLogger(AlertDao.class);

	private AlertDao() {

	}

	public  Boolean saveAlert(Alert alert) {

		Session session = Helper.getSession();
		try {
			session.getTransaction().begin();
			session.save(alert);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		} finally {
			session.close();
		}
	}

	public  Alert getAlert(int id) {

		Session session = Helper.getSession();
		try {
			Alert  alert = (Alert) session.get(Alert.class, id);
			alert.setStatus(Status.VIEWED);
			session.update(alert);
			session.beginTransaction().commit();

			return alert;
		} catch (Exception e) {
			logger.info(e);
			return null;
		} finally {
			session.close();
		}
	}

	public  List<Alert> getAllAlertsPerReceiver(int id) {
		Session session = Helper.getSession();
		try {
			session.getTransaction().begin();
			String hql = "FROM Alert a where a.toId=?";
			Query query = session.createQuery(hql);
			query.setParameter(0, session.get(User.class, id));
			@SuppressWarnings("unchecked")
			List<Alert> results = query.list();

			return results;
		} catch (Exception e) {
			logger.info(e);

		} finally {
			session.close();
		}
		return Collections.emptyList();

	}

	public  Alert getAlertAndUpdate(int id, String status) {
		String status1 = "accept";
		String status2 = "decline";
		Session session = Helper.getSession();
		try {
			Alert alert = (Alert) session.get(Alert.class, id);
			if (status.equals(status1)) {
				alert.setStatus(Status.ACCEPT);
				session.update(alert);
				session.beginTransaction().commit();
			}
			if (status.equals(status2)) {
				alert.setStatus(Status.DECLINE);
				session.update(alert);
				session.beginTransaction().commit();
			}
			return alert;
		} catch (Exception e) {
			logger.info(e);
		} finally {
			session.close();
		}
		return null;
	}

}
