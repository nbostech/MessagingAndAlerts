package com.wavelabs.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.wavelabs.model.User;
import com.wavelabs.utility.Helper;
@Component
public class UserDao {
	static Logger logger = Logger.getLogger(UserDao.class);
	

	private UserDao() {

	}

	public  User getUser(int id) {
		
		Session session = Helper.getSession();
		User user = (User) session.get(User.class, id);
		session.close();
		return user;
	}

	public  boolean saveUser(User user) {

		Session session = Helper.getSession();
		try {
			session.getTransaction().begin();
			session.save(user);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		} finally {
			session.close();
		}
	}


	public  boolean deleteUser(int id) {
		Session session = Helper.getSession();
		try {
			session.getTransaction().begin();
			User user = (User) session.get(User.class, id);
			session.delete(user);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		} finally {
			session.close();

		}

	}
}
