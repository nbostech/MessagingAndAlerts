package com.wavelabs.dao;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import com.wavelabs.model.Message;
import com.wavelabs.model.MessageThread;
import com.wavelabs.model.User;
import com.wavelabs.utility.Helper;

@Component
public class MessageDao {
	static Logger log = Logger.getLogger(MessageDao.class);

	private MessageDao() {

	}

	public Message[] getConversations(int id) {
		Session session = Helper.getSession();
		User toId = (User) session.get(User.class, id);
		String hql = "select distinct messagethread from " + Message.class.getName() + " where to_Id=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, toId);
		@SuppressWarnings("unchecked")
		List<MessageThread> mts = query.list();
		Message[] messages = new Message[mts.size()];
		int j = 0;
		for (MessageThread mt : mts) {
			Query query2 = session
					.createQuery(" from " + Message.class.getName() + " where timeStamp = (select max(timeStamp) from "
							+ Message.class.getName() + " where messagethread=:mt)")
					.setParameter("mt", mt);
			messages[j++] = (Message) query2.uniqueResult();
		}
		session.close();
		return messages;
	}

	public List<Message> getConversationMessages(int threadId) {

		Session session = Helper.getSession();
		try {
			String hql = "from " + Message.class.getName() + " where messagethread=?";
			Query query = session.createQuery(hql);
			MessageThread mt = (MessageThread) session.get(MessageThread.class, threadId);
			query.setParameter(0, mt);
			@SuppressWarnings("unchecked")
			List<Message> messages = query.list();
			mt.setIsread(true);
			for (Message m : messages) {
				log.info(m.getBody());

			}
			return messages;
		} catch (Exception e) {
			log.info(e);
			return Collections.<Message>emptyList();
		} finally {
			session.close();
		}

	}

	public MessageThread getMessagesAndUpdate(int id, String isRead) {

		Session session = Helper.getSession();
		try {
			MessageThread mt = (MessageThread) session.get(MessageThread.class, id);
			String status = "true";
			if (isRead.equals(status)) {
				mt.setIsread(true);
				session.update(mt);
				session.beginTransaction().commit();

			}
			return mt;

		} catch (Exception e) {
			log.info(e);
			return null;
		} finally {
			session.close();
		}
	}

	public boolean saveMessage(Message message) {
		Session session = Helper.getSession();
		try {
			MessageThread mt = message.getMessagethread();
			message.setTimeStamp(Calendar.getInstance());

			if (mt == null) {
				mt = new MessageThread();
				Set<User> users = new HashSet<>();
				User from = message.getFromId();
				User to = message.getTo_Id();
				users.add(from);
				users.add(to);
				mt.setIsread(false);
				mt.setUser(users);
				session.save(mt);
				message.setMessagethread(mt);
			} else {
				MessageThread fromDb = (MessageThread) session.get(MessageThread.class,
						message.getMessagethread().getId());
				fromDb.setIsread(mt.isIsread());
				message.setMessagethread(fromDb);
			}
			session.save(message);
			session.beginTransaction().commit();
			return true;
		} catch (Exception e) {
			log.info(e);
			return false;
		} finally {
			session.close();
		}
	}

	public boolean deleteUserConversation(int userId, int messageThreadId) {
		Session session = Helper.getSession();
		try {
			Transaction tx = session.beginTransaction();
			MessageThread thread = (MessageThread) session.get(MessageThread.class, messageThreadId);
			Set<User> users = thread.getUser();
			for (User user : users) {
				if (user.getId() == userId) {
					users.remove(user);
					break;
				}
			}
			thread.setUser(users);
			session.update(thread);
			tx.commit();
			return true;
		} catch (Exception e) {
			log.info(e);
			return false;
		} finally {
			session.close();
		}
	}

	public Boolean deleteMessage(Integer id) {
		Session session = Helper.getSession();
		try {
			Transaction tx = session.beginTransaction();
			Message message = (Message) session.get(Message.class, id);
			session.delete(message);
			session.save(message);
			tx.commit();
			return true;
		} catch (Exception e) {
			log.info(e);
			return false;
		} finally {
			session.close();
		}
	}

}
